Feeds Data
Original author: Caleb Thorne (draenen) <calebt@monarchdigital.com>
Date: Feb 2015

DESCRIPTION
------------

The Feeds Data module defines a feeds processor to import feeds into tables
managed by the Data module. The base feeds processor (FeedsProcessor) assumes
the target data is an entity which may not be true for data tables. This module
provides a custom feeds processor plugin to manage tables that:

  * Are not associated with a Drupal entity.
  * May have multiple Primary Keys.

REQUIREMENTS
------------

This module requires the following modules:
 * Data (https://drupal.org/project/data)
 * Feeds (https://drupal.org/project/feeds)

CONFIGURATION
-------------

 * Create or adopt a data table at Administration » Structure » Data tables
 * Create a feeds importor using the Data Processor at
   Administration » Structure » Feeds importers
